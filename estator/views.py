import os

from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.http import Http404
from django.conf import settings
from random import randrange

from models import *



COMPANY = {
     'COMPANY_NAME':'ESTATOR',
     'COMPANY_SLOGAN' : 'ZIkkimin dibi!',
     'MEDIA_URL':settings.MEDIA_URL,
     }



def get_random_objects(obj, count=3):

    db_count = obj.objects.count()
    if db_count == 0:
        return None

    id_set = set()

    if count > db_count: count = db_count
    while len(id_set) < count:
        id = randrange(1, db_count+1)


        id_set.add(id)

        objs = []
        for i in id_set:
            objs.append(Rental.objects.get(id=i))

    return objs



def home(request):

    photo_obj_set =  get_random_objects(Rental, 3)
    
    rentals = Rental.objects.all()
    for_sales = Sales.objects.all()

    c = {
        'photos':photo_obj_set,
        'rental':rentals,
        'for_sale':for_sales,
        }
    
    c.update(COMPANY)
    
    
    return render_to_response('home.html', c)



def rentals(request):

    qset = Rental.objects.all()

    
    c = {
        'rental': qset, 
        }
    c.update(COMPANY)

    return render_to_response("rentals.html", c)



def for_sales(request):
    qset = Sales.objects.all()

    c = {
        'for_sales' : qset,
        }
    c.update(COMPANY)

    return render_to_response("for_sales.html", c)


def rentals_detail(request, id):

    if request.method == 'GET':
        try:
            # do the thing
            q = Rental.objects.get(id=id)
        except:
            raise Http404

        c = {
            'rental':q,
            }

        c.update(COMPANY)

        return render_to_response("rentals_detail.html", c)
    return HttpResponse(code=404)




def for_sales_detail(request):

    if request.method == 'GET':
        try:
            # do the thing
            pass
        except:
            return HttpResponse(code=404)

        c = {
            }

        c.update(COMPANY)

        return render_to_response("rentals_detail.html", c)
    return HttpResponse(code=404)
    
