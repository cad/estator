from estator.models import HouseBussiness
from estator.models import Photos
from estator.models import Rental
from estator.models import Sales
from estator.models import Properties
from estator.models import Settings
from django.contrib import admin
from django.http import HttpResponseRedirect
from django.http import HttpRequest



class HouseBussinessAdmin(admin.ModelAdmin):

	# def response_add(self, request, obj, post_url_continue=None):
	# 	return HttpResponseRedirect("/admin/estator/photos/add")

	list_display = ('id','room','saloon','area','address','date')

class PhotosAdmin(admin.ModelAdmin):
	# def response_add(self, request, obj, post_url_continue=None):
	# 	return HttpResponseRedirect("/admin/estator/properties/add")

	list_display = ('photo',)

class PropertiesAdmin(admin.ModelAdmin):
        # def response_add(self, request, obj, post_url_continue=None):
			
        pass
        #         return HttpResponseRedirect("/admin/estator/rental/add")

class RentalAdmin(admin.ModelAdmin):
        # def response_add(self, request, obj, post_url_continue=None):
        #         return HttpResponseRedirect("/admin/estator/sales/add")

        pass
 
class SalesAdmin(admin.ModelAdmin):
        # def response_add(self, request, obj, post_url_continue=None):
        #         return HttpResponseRedirect("/admin/estator")
        pass

admin.site.register(HouseBussiness, HouseBussinessAdmin)
admin.site.register(Photos, PhotosAdmin)
admin.site.register(Rental, RentalAdmin)
admin.site.register(Sales, SalesAdmin)
admin.site.register(Properties, PropertiesAdmin)
admin.site.register(Settings)
