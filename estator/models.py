from django.db import models
from django.conf import settings
import uuid
import os

class HouseBussiness(models.Model):

	class Meta:
		verbose_name_plural = "Houses and Bussiness"
	def __unicode__(self):
		return str(self.id)
	
	TYPES = (
		('FL','Flat'),
		('MH','Mountain House'),	
		('P','Pavilion'),
		('VH','Village House'),
		('V','Villa'),
		('M','Mansion'),
		('SH','SummerHouse')
		)

		
	
	area = models.IntegerField(verbose_name="How many square metre?")
	room = models.IntegerField("How many rooms?")
	saloon = models.IntegerField("How many saloons?")
	date = models.DateTimeField()
	typ = models.CharField("Type of home",max_length=2, choices=TYPES)
	age = models.IntegerField("Age of building")
	numberOfFloors = models.IntegerField("How many floors?")
	whichFloor = models.IntegerField("At which floor")
	address = models.TextField()
	description = models.TextField()
        properties = models.ForeignKey('Properties')
        photo = models.ForeignKey('Photos', verbose_name="Please select a home from id" )
        

class Photos(models.Model):

	class Meta:
		verbose_name_plural = "Photos"
	
	def __unicode__(self):
		return str(self.photo)
	
	# uid = uuid.uuid1()
	# uid = uid.__str__()

	photo = models.ImageField(upload_to=settings.MEDIA_ROOT)



        def fixed_url(self):
                return os.path.split(self.photo.name)[1]

class Rental(models.Model):	
	TYPEOFRENTS = (
		('Y','Yearly'),
		('M','Monthly'),
		('6M','6 Month'),
		)
	house = models.ForeignKey(HouseBussiness, verbose_name="Please select a home from id")
	typeOfRent = models.CharField(max_length=2,choices=TYPEOFRENTS)
	cost = models.IntegerField()

class Sales(models.Model):

	class Meta:
		verbose_name_plural = "Sales"
	house = models.ForeignKey(HouseBussiness, verbose_name="Please select a home from id")
	cost = models.IntegerField()

class Properties(models.Model):
	
	class Meta:
		verbose_name_plural = "Properties"

	#propId = models.ForeignKey(HouseBussiness, verbose_name="Please select a home from id")
	internet = models.BooleanField()
	lift = models.BooleanField()
	showerCabin = models.BooleanField()
	generator = models.BooleanField()
	plasterBoard = models.BooleanField()
	hardWood = models.BooleanField()
	alarm = models.BooleanField()
	satellite = models.BooleanField()
	laminate = models.BooleanField()
	underfloorHeating = models.BooleanField()
	steelDoor = models.BooleanField()
	intercom = models.BooleanField()
	jacuzzi = models.BooleanField()
	porter = models.BooleanField()
	sauna = models.BooleanField()
	security = models.BooleanField()
	fireEscape = models.BooleanField()
	parking = models.BooleanField()

class Settings(models.Model):
	
	class Meta:
		verbose_name_plural = "Settings"

	title = models.CharField(max_length=200)
	nameOfCompany = models.CharField(max_length=200)	
	slogan = models.CharField(max_length=300)	
	footer = models.CharField(max_length=200)
	logo = models.ImageField(upload_to='photos/logo')
