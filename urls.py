from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
# Uncomment the next two lines to enable the admin:

from django.conf import settings
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'estate.views.home', name='home'),
    # url(r'^estate/', include('estate.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^home/', 'estator.views.home'),
                       
    url(r'^rentals/', 'estator.views.rentals'),
    url(r'^forsales/', 'estator.views.for_sales'),


    url(r'^rental/(?P<id>\d+)/$', 'estator.views.rentals_detail'),
    url(r'^forsale/(?P<id>\d+)/$', 'estator.views.for_sales_detail'),

                       

                       
             
) + staticfiles_urlpatterns()

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
   )
